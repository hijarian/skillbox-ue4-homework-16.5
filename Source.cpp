#include <chrono>
#include <iostream>
#include <iomanip>

int get_day_of_month(const std::chrono::system_clock::time_point& point_of_reference)
{
	auto now{ std::chrono::current_zone()->to_local(point_of_reference) };
	auto today{ std::chrono::floor<std::chrono::days>(now) };
	auto first_day_of_month{ std::chrono::floor<std::chrono::days>(std::chrono::floor<std::chrono::months>(now)) };
	auto days_since_beginning_of_month{ std::chrono::duration_cast<std::chrono::days>(today - first_day_of_month) };

	return days_since_beginning_of_month.count() + 1; // current day of month = days since the beginning from month + 1
}

int main()
{
	const unsigned int N = 13;

	unsigned int array[N][N]{};
	for (unsigned int i = 0; i < N; ++i)
	{
		for (unsigned int j = 0; j < N; ++j)
		{
			array[i][j] = i + j;
		}
	}

	for (unsigned int i = 0; i < N; ++i)
	{
		for (unsigned int j = 0; j < N; ++j)
		{
			std::cout << std::setw(2) << array[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	const int day_of_month = get_day_of_month(std::chrono::system_clock::now());
	const int row_index = day_of_month % N;
	std::cout << "Our special row with index ";
	std::cout << day_of_month << " % " << N << " = " << row_index;
	std::cout << ":" << std::endl;

	for (unsigned int j = 0; j < N; ++j)
	{
		std::cout << array[row_index][j] << " ";
	}
	std::cout << std::endl;
}